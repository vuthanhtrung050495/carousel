import React from "react";
import { View, FlatList, Text } from "react-native";
import { graphql, Query } from "react-apollo";
import { CachedImage, ImageCacheProvider } from "react-native-cached-image";
import styles from "./styles";
import query from "../../query/carousel";

class Carousel extends React.PureComponent {
  renderCarousel(data) {
    return (
      <View style={styles.container}>
        {data.viewer && (
          <ImageCacheProvider
            urlsToPreload={data.viewer.images.map(item => item.url)}
            onPreloadComplete={() => console.log("load completed")}
          >
            <FlatList
              data={data.viewer.images}
              keyExtractor={item => item.id}
              extraData={this.state}
              horizontal
              initialNumToRender={4}
              renderItem={({ item }) => (
                <View style={styles.imageView}>
                  <View style={styles.image}>
                    <CachedImage
                      source={{ uri: item.url }}
                      style={styles.image}
                    />
                  </View>
                </View>
              )}
            />
          </ImageCacheProvider>
        )}
      </View>
    );
  }

  render() {
    return (
      <Query query={query} errorPolicy="all">
        {({ error, data, loading }) => {
          if (loading)
            return (
              <View style={styles.container}>
                <Text style={styles.textCarousel}>Loading...</Text>
              </View>
            );
          if (error)
            return (
              <View style={styles.container}>
                <Text style={styles.textCarousel}>Unable To Load Data</Text>
              </View>
            );
          return this.renderCarousel(data);
        }}
      </Query>
    );
  }
}

export default graphql(query)(Carousel);
