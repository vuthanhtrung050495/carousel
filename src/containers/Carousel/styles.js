import { deviceWidth } from "../../constants";

export default {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black"
  },
  imageView: {
    flex: 1,
    justifyContent: "center"
  },
  image: {
    width: deviceWidth / 4 - 10,
    height: 80,
    marginRight: 10
  },
  textCarousel: {
    color: "white",
    fontSize: 20
  }
};
