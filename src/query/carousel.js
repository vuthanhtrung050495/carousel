import gql from "graphql-tag";

const query = gql`
  query {
    viewer {
      images {
        id
        url
      }
    }
  }
`;

export default query;
