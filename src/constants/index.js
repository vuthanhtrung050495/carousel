import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const graphqlURL = "https://avo-stg.avovietnam.com/graphql";

export const deviceWidth = width;

export const deviceHeight = height;
