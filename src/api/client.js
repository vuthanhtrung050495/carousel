import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from "apollo-cache-inmemory";
import { onError } from "apollo-link-error";
import { Alert } from "react-native";
import { ApolloLink } from "apollo-link";

import { graphqlURL as uri } from "../constants";

const errorLink = onError(({ networkError, graphQLErrors }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      Alert.alert(
        "",
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    );
  }
  if (networkError) Alert.alert("", `[Network error]: ${networkError}`);
});

const httpLink = createHttpLink({
  errorLink,
  uri
});

const link = ApolloLink.from([errorLink, httpLink]);

const authLink = setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      Authorization: "dev"
    }
  };
});

const client = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache()
});

export default client;
