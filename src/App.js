import React from "react";
import { ApolloProvider } from "react-apollo";

import client from "./api/client";
import Carousel from "./containers/Carousel";

console.disableYellowBox = true;

const App = () => (
  <ApolloProvider client={client}>
    <Carousel />
  </ApolloProvider>
);

export default App;
